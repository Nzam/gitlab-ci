stages:
  - test
  - build
  - deploy_dev
  - deploy_staging
  - deploy_prod

workflow:
  rules:
    - if: $CI_COMMIT_BRANCH != "main" && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: never
    - when: always

run_unit_tests:
  tags:
    - docker-executor
  image: node:17-alpine3.14
  stage: test
  before_script:
    - cd app
    - npm install
  script:
    - npm test
  artifacts:
    when: always
    paths:
      - app/junit.xml
    reports:
      junit:
        - app/junit.xml
sast:
  stage: test

variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE
  DEV_SERVER_HOST: "3.111.246.143"
  STAGING_SERVER_HOST: "3.111.246.143"
  PROD_SERVER_HOST: "3.111.246.143"
  DEV_ENDPOINT: http://ec2-3-111-246-143.ap-south-1.compute.amazonaws.com:3000
  STAGING_ENDPOINT: http://ec2-3-111-246-143.ap-south-1.compute.amazonaws.com:4000
  PROD_ENDPOINT: http://ec2-3-111-246-143.ap-south-1.compute.amazonaws.com:5000

build_image:
  tags:
    - ec2-shell_executor
  stage: build
  cache:
    key: $CI_COMMIT_REF_NAME
    paths:
      - app/node_modules/

  before_script:
    - echo "Linux user is $USER"
    - echo "Docker registry user is $CI_REGISTRY_USER"
    - echo "Docker registry name is $CI_REGISTRY"
    - echo "Docker registry image is $CI_REGISTRY_IMAGE"
    - docker info
    - export PACKAGE_JSON_VERSION=$(cat app/package.json | jq -r .version)
    - export VERSION=$PACKAGE_JSON_VERSION.$CI_PIPELINE_IID
    - echo $VERSION > version-file.txt
  script:
    - docker build -t $IMAGE_NAME:$VERSION .
  artifacts:
    paths:
      - version-file.txt  

push_image:
  tags:
    - ec2-shell_executor
  stage: build
  needs:
    - build_image 
  before_script:
    - export VERSION=$(cat version-file.txt)
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker push $IMAGE_NAME:$VERSION

.deploy:
  variables:
    SSH_KEY: ""
    SERVER_HOST: ""
    DEPLOY_ENV: ""
    APP_PORT: ""
    ENDPOINT: ""
  tags:
    - ec2-shell_executor
  stage: deploy_dev
  before_script:
    - chmod 400 $SSH_PRIVATE_KEY 
    - export VERSION=$(cat version-file.txt)
  script:
    - scp -o StrictHostKeyChecking=no -i "$SSH_KEY" ./docker-compose.yaml ec2-user@"$SERVER_HOST":/home/ec2-user
    - ssh -o StrictHostKeyChecking=no -i "$SSH_KEY" ec2-user@"$SERVER_HOST" 
      "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY &&
      export COMPOSE_PROJECT_NAME=$DEPLOY_ENV &&
      export DC_IMAGE_NAME=$IMAGE_NAME &&
      export DC_IMAGE_TAG=$VERSION &&
      export DC_APP_PORT=$APP_PORT &&
      docker-compose down && docker-compose up -d"

  environment:
    name: $DEPLOY_ENV
    url: $ENDPOINT



deploy_to_dev:
  extends: .deploy
  stage: deploy_dev
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SERVER_HOST: $DEV_SERVER_HOST
    DEPLOY_ENV: development
    APP_PORT: 3000
    ENDPOINT: $DEV_ENDPOINT

run_functional_tests:
  stage: deploy_dev
  needs:
    - deploy_to_dev
  script:
    - echo "running functional tests"

deploy_to_staging:
  extends: .deploy
  stage: deploy_staging
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SERVER_HOST: $STAGING_SERVER_HOST
    DEPLOY_ENV: staging
    APP_PORT: 4000
    ENDPOINT: $STAGING_ENDPOINT

run_performance_tests:
  stage: deploy_staging
  needs:
    - deploy_to_staging
  script: 
    - echo "Running performance tests"

deploy_to_prod:
  extends: .deploy
  stage: deploy_prod
  variables:
    SSH_KEY: $SSH_PRIVATE_KEY
    SERVER_HOST: $PROD_SERVER_HOST
    DEPLOY_ENV: production
    APP_PORT: 5000
    ENDPOINT: $PROD_ENDPOINT
  when: manual


include:
  - template: Jobs/SAST.gitlab-ci.yml
